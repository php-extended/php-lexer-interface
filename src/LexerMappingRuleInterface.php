<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

use Stringable;

/**
 * LexerMappingRuleInterface interface file.
 * 
 * A Mapping Rule is a rule that maps a given character on a given Lexeme code.
 * 
 * @author Anastaszor
 */
interface LexerMappingRuleInterface extends Stringable
{
	
	/**
	 * The character that represents a lexeme.
	 * 
	 * @return string
	 */
	public function getValue() : string;
	
	/**
	 * Gets the code of the lexeme.
	 * 
	 * @return integer
	 */
	public function getCode() : int;
	
}
