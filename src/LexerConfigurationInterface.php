<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

use Iterator;
use PhpExtended\Charset\CharacterSetInterface;
use Stringable;

/**
 * LexerConfigurationInterface interface file.
 * 
 * A configuration is the set of rules that the parser should follow to generate
 * its lexemes.
 * 
 * @author Anastaszor
 */
interface LexerConfigurationInterface extends Stringable
{
	
	/**
	 * Sets the charset that is used by this configuration for the lexing.
	 * 
	 * @param CharacterSetInterface $charset
	 */
	public function setCharset(CharacterSetInterface $charset) : LexerConfigurationInterface;
	
	/**
	 * Gets the charset that represents the input stream.
	 * 
	 * @return CharacterSetInterface
	 */
	public function getCharset() : CharacterSetInterface;
	
	/**
	 * Adds a new mapping rules to this configuration. The given string can be
	 * a single character, or all the characters of a given class.
	 * 
	 * It is advised to create classes based on integer constants that represent
	 * a full range of characters. The LExerInterface contains default classes
	 * that can be used in this configuration to parse data.
	 *
	 * @param string $string the characters to be mapped together
	 * @param integer $code the token code of the mapping
	 */
	public function addMappings(string $string, int $code) : LexerConfigurationInterface;
	
	/**
	 * Gets the mapping rules.
	 * 
	 * @return Iterator<LexerMappingRuleInterface>
	 */
	public function getMappingRules() : Iterator;
	
	/**
	 * Adds a new merging rule to this configuration.
	 *
	 * @param integer $before the code of the current lexeme
	 * @param integer $after the code of the future lexeme
	 * @param integer $new the code of the merged lexeme
	 */
	public function addMerging(int $before, int $after, int $new) : LexerConfigurationInterface;
	
	/**
	 * Gets the merging rules.
	 * 
	 * @return Iterator<LexerMergingRuleInterface>
	 */
	public function getMergingRules() : Iterator;
	
}
