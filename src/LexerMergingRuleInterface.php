<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

use Stringable;

/**
 * LexerMergingRuleInterface interface file.
 * 
 * A Merging Rule is a rule that transforms a before lexeme and an after lexeme
 * into a new lexeme with the full data and a new code.
 * 
 * @author Anastaszor
 */
interface LexerMergingRuleInterface extends Stringable
{
	
	/**
	 * Gets the code of the bofore lexeme.
	 * 
	 * @return integer
	 */
	public function getBeforeCode() : int;
	
	/**
	 * Gets the code of the after lexeme for it to be joined.
	 * 
	 * @return integer
	 */
	public function getAfterCode() : int;
	
	/**
	 * Gets the code of the transformed lexeme, after the joint of the before
	 * lexeme and the after lexeme.
	 * 
	 * @return integer
	 */
	public function getNewCode() : int;
	
}
