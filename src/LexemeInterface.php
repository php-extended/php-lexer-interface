<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

use Stringable;

/**
 * LexemeInterface interface file.
 * 
 * A Lexeme is a small token that represents an unit data that is parsed by
 * the parser.
 * 
 * @author Anastaszor
 */
interface LexemeInterface extends Stringable
{
	
	/**
	 * The code of the lexeme. Should be a constant in the form of L_xxx that
	 * is an integer. Each real parser should define its constants as positive
	 * integers.
	 * 
	 * @return integer
	 */
	public function getCode() : int;
	
	/**
	 * Gets the line number of the lexeme that represents its start. A new line
	 * is counted for each \r\n ; each \r ; and each \n that the Lexer finds.
	 * 
	 * @return integer
	 */
	public function getLine() : int;
	
	/**
	 * Gets the column number of the lexeme that represents its start. The
	 * column number restarts at 0 for each line that is found.
	 * 
	 * @return integer
	 */
	public function getColumn() : int;
	
	/**
	 * The full data of the lexeme. Should be a non-empty string.
	 * 
	 * @return string
	 */
	public function getData() : string;
	
}
