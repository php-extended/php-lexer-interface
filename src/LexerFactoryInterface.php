<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

use PhpExtended\Io\InputStreamInterface;
use Psr\Http\Message\StreamInterface;
use SplFileObject;
use Stringable;

/**
 * LexerFactoryInterface interface file.
 * 
 * This factory creates lexer from the given configuration.
 * 
 * @author Anastaszor
 */
interface LexerFactoryInterface extends Stringable
{
	
	/**
	 * Creates a new lexer with the given configuration and data.
	 * 
	 * @param string $data
	 * @param ?LexerConfigurationInterface $config
	 * @return LexerInterface
	 */
	public function createFromString(string $data, ?LexerConfigurationInterface $config) : LexerInterface;
	
	/**
	 * Creates a new lexer with the given configuration and file.
	 * 
	 * @param SplFileObject $file
	 * @param ?LexerConfigurationInterface $config
	 * @return LexerInterface
	 */
	public function createFromFileObject(SplFileObject $file, ?LexerConfigurationInterface $config) : LexerInterface;
	
	/**
	 * Creates a new lexer with the given configuration and stream.
	 * 
	 * @param StreamInterface $stream
	 * @param ?LexerConfigurationInterface $config
	 * @return LexerInterface
	 */
	public function createFromStream(StreamInterface $stream, ?LexerConfigurationInterface $config) : LexerInterface;
	
	/**
	 * Creates a new lexer with the given input stream.
	 * 
	 * @param InputStreamInterface $stream
	 * @param ?LexerConfigurationInterface $config
	 * @return LexerInterface
	 */
	public function createFromInput(InputStreamInterface $stream, ?LexerConfigurationInterface $config) : LexerInterface;
	
}
