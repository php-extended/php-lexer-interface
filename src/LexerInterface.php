<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

use Iterator;
use Stringable;

/**
 * LexerInterface interface file.
 * 
 * This interface represents a generic lexer for any data.
 * 
 * @author Anastaszor
 * @extends \Iterator<int, LexemeInterface>
 */
interface LexerInterface extends Iterator, Stringable
{
	
	/**
	 * Lexeme Id for the end of stream.
	 * 
	 * @var integer
	 */
	public const L_EOS = -1;

	/**
	 * Lexeme Id for the data that is not recognized by the rules, thus is
	 * considered as trash.
	 * 
	 * @var integer
	 */
	public const L_TRASH = -2;
	
	/**
	 * Special Lexeme Id in last resort if the data is not found in the mapping
	 * or merging classes to map it to a merged lexeme. Everything includes
	 * Trash, but does not includes EndOfStream.
	 * 
	 * @var integer
	 */
	public const L_EVERYTHING_ELSE = -3;
	
	/**
	 * A class to represent all digits in the mappings.
	 * 
	 * @var string
	 */
	public const CLASS_DIGIT = '0123456789';
	
	/**
	 * A class to represent hexadecimal characters in the mappings.
	 * 
	 * @var string
	 */
	public const CLASS_HEXA = '0123456789ABCDEFabcdef';
	
	/**
	 * A class to represent alphabetical characters in the mappings.
	 * 
	 * @var string
	 */
	public const CLASS_ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	
	/**
	 * A class to represent alphanumerical characters in the mappings.
	 * 
	 * @var string
	 */
	public const CLASS_ALNUM = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	
	/**
	 * A class to represent whitespace characters in the mappings.
	 * 
	 * @var string
	 */
	public const CLASS_SPACE = " \t\n\r\x0B";
	
	/**
	 * A class to represent utf-8 whitespace codepoints in the mappings.
	 * 
	 * @var string
	 */
	public const CLASS_UTF8WS = "\u{00A0}\u{1680}\u{180E}\u{2000}\u{2001}\u{2002}\u{2003}\u{2004}\u{2005}\u{2006}\u{2007}\u{2008}\u{2009}\u{200A}\u{200B}\u{200C}\u{200D}\u{2028}\u{2029}\u{202F}\u{205F}\u{2060}\u{3000}\u{FEFF}";
	
	/**
	 * A class to represent (ascii/utf8) control characters in the mappings.
	 * 
	 * @var string
	 */
	public const CLASS_CONTROL = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x0C\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F\x7F";
	
	/**
	 * A class to represent utf-8 supplemental latin 1 control characters.
	 * 
	 * @var string
	 */
	public const CLASS_UTF8SLC = "\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8A\x8B\x8C\x8D\x8E\x8F\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9A\x9B\x9C\x9D\x9E\x9F";
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 */
	public function current() : LexemeInterface;
	
}
